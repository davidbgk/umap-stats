from itertools import cycle
from pathlib import Path

import httpx
import minicli
import tomllib


def get_latest_version(dependency):
    if "==" in dependency:
        return dependency.split("==", 1)
    else:
        version = "latest"
    url = f"https://unpkg.com/browse/{dependency}@{version}/"
    response = httpx.head(url)
    if response.status_code >= 400:
        response.raise_for_status()
    return dependency, response.headers["Location"].split("@")[-1].strip("/")


def download_file(dependency, version, kind):
    dependency_name = dependency.split("/")[-1]
    url = f"https://unpkg.com/{dependency}@{version}/dist/{dependency_name}.{kind}"
    print(f"Trying URL: {url}")
    response = httpx.get(url)
    try:
        response.raise_for_status()
    except httpx.HTTPStatusError:
        url = f"https://unpkg.com/{dependency}@{version}/dist/{dependency_name}.esm.{kind}"
        print(f"Trying URL: {url}")
        response = httpx.get(url)
        try:
            response.raise_for_status()
        except httpx.HTTPStatusError:
            url = f"https://unpkg.com/{dependency}@{version}/{kind}/{dependency_name}.{kind}"
            print(f"Trying URL: {url}")
            response = httpx.get(url)
            try:
                response.raise_for_status()
            except httpx.HTTPStatusError:
                url = f"https://unpkg.com/{dependency}@{version}/{kind}/{dependency_name}.min.{kind}"
                print(f"Trying URL: {url}")
                response = httpx.get(url)
    return response.text, url


def save_file(dependency, version, file_content, kind):
    target_path = Path() / "static" / "vendors"
    target_path.mkdir(parents=True, exist_ok=True)
    dependency_name = "-".join(part.strip("@") for part in dependency.split("/"))
    target_filename = target_path / f"{dependency_name}.{version}.{kind}"
    target_filename.write_text(file_content)
    return target_filename


@minicli.cli
def install():
    pyproject = tomllib.loads((Path() / "pyproject.toml").read_text())
    js_dependencies = pyproject["tool"]["npm"]["js"]
    js_kind_dependencies = list(zip(cycle(["js"]), js_dependencies))
    css_dependencies = pyproject["tool"]["npm"]["css"]
    css_kind_dependencies = list(zip(cycle(["css"]), css_dependencies))
    for kind, dependency in js_kind_dependencies + css_kind_dependencies:
        name, version = get_latest_version(dependency)
        file_content, url = download_file(name, version, kind)
        file_name = save_file(name, version, file_content, kind)
        print(f"{name}, {version}, saved in {file_name} from {url}")


if __name__ == "__main__":
    minicli.run()
