# umap-stats

## Installation

Pré-requis : Python3

Installer et activer un environnement virtuel :

    $ python3 -m venv venv
    $ source venv/bin/activate

Installer les dépendances :

    $ make install

## Lancement

Lancer le serveur local :

    $ make build serve

Aller sur http://127.0.0.1:8000/

