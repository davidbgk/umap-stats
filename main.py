import json
from collections import defaultdict
from datetime import datetime
from pathlib import Path

import httpx
import minicli
from jinja2 import Environment as Env
from jinja2 import FileSystemLoader
from slugify import slugify

INSTANCES = [
    ("ANCT", "umap.incubateur.anct.gouv.fr"),
    ("OSM France", "umap.openstreetmap.fr"),
    ("OSM Suisse", "umap.osm.ch"),
    ("OSM Allemagne", "umap.openstreetmap.de"),
    ("Framacarte", "framacarte.org"),
]

DATA = Path() / "data"
PUBLIC = Path() / "public"
TEMPLATES = Path() / "templates"

environment = Env(loader=FileSystemLoader(str(TEMPLATES)))
environment.filters["slugify"] = lambda a: slugify(a)
environment.filters["large_number"] = lambda a: f"{a:,}"


@minicli.cli
def download_stats():
    for name, url in INSTANCES:
        stats_url = f"http://{url}/stats/"
        response = httpx.get(stats_url, follow_redirects=True)
        path_folder = DATA / url
        path_folder.mkdir(parents=True, exist_ok=True)
        path_name = (
            f"{datetime.today().isoformat(timespec='minutes').replace(':', '-')}.json"
        )
        (path_folder / path_name).write_text(response.text)


def extract_latest():
    latest = {}
    for name, url in INSTANCES:
        for stat_file in reversed(list(each_file_from(DATA / url, pattern="*.json"))):
            data = json.loads(stat_file.read_text())
            latest[name] = data
            break
    return latest


def extract_labels(source=INSTANCES[0][1]):
    labels = []
    for stat_file in each_file_from(DATA / source, pattern="*.json"):
        labels.append(str(stat_file.stem).split("T", 1)[0])
    return labels


def extract_datasets(key="maps_count"):
    datasets = []
    cumulative_values = defaultdict(int)
    for name, url in INSTANCES:
        values = []
        for i, stat_file in enumerate(each_file_from(DATA / url, pattern="*.json")):
            data = json.loads(stat_file.read_text())
            values.append(data[key])
            cumulative_values[i] += data[key]
        datasets.append({"name": name, "chartType": "line", "values": values})
    datasets.append(
        {
            "name": "Cumulative",
            "chartType": "line",
            "values": list(cumulative_values.values()),
        }
    )
    return datasets


@minicli.cli
def build():
    template = environment.get_template("base.html")
    latest = extract_latest()
    total_users_count = sum(data["users_count"] for instance, data in latest.items())
    total_maps_count = sum(data["maps_count"] for instance, data in latest.items())
    maps_count_datasets = extract_datasets(key="maps_count")
    maps_active_last_week_count_datasets = extract_datasets(
        key="maps_active_last_week_count"
    )
    users_count_datasets = extract_datasets(key="users_count")
    users_active_last_week_count_datasets = extract_datasets(
        key="users_active_last_week_count"
    )
    instances = {"Cumulative": {"url": "all", "labels": extract_labels()}}
    for [name, url] in INSTANCES:
        instances[name] = {"url": url, "labels": extract_labels(url)}
    content = template.render(
        instances=instances,
        latest=latest,
        total_users_count=total_users_count,
        total_maps_count=total_maps_count,
        maps_count_datasets=maps_count_datasets,
        maps_active_last_week_count_datasets=maps_active_last_week_count_datasets,
        users_count_datasets=users_count_datasets,
        users_active_last_week_count_datasets=users_active_last_week_count_datasets,
    )
    (PUBLIC / "index.html").write_text(content)

    cumulative_maps_count = next(
        dataset["values"][-1]
        for dataset in maps_count_datasets
        if dataset["name"] == "Cumulative"
    )
    cumulative_maps_active_count = next(
        dataset["values"][-1]
        for dataset in maps_active_last_week_count_datasets
        if dataset["name"] == "Cumulative"
    )
    cumulative_users_count = next(
        dataset["values"][-1]
        for dataset in users_count_datasets
        if dataset["name"] == "Cumulative"
    )
    cumulative_users_active_count = next(
        dataset["values"][-1]
        for dataset in users_active_last_week_count_datasets
        if dataset["name"] == "Cumulative"
    )
    api_data = {
        "maps_count": cumulative_maps_count,
        "maps_active_count": cumulative_maps_active_count,
        "users_count": cumulative_users_count,
        "users_active_count": cumulative_users_active_count,
    }
    (PUBLIC / "data.json").write_text(json.dumps(api_data, indent=2))


def each_file_from(source_dir, pattern="*", exclude=None):
    """Walk across the `source_dir` and return the `pattern` file paths."""
    for path in _each_path_from(source_dir, pattern=pattern, exclude=exclude):
        if path.is_file():
            yield path


def _each_path_from(source_dir, pattern="*", exclude=None):
    for path in sorted(Path(source_dir).glob(pattern)):
        if exclude is not None and path.name in exclude:
            continue
        yield path


if __name__ == "__main__":
    minicli.run()
